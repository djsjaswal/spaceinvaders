package spaceinvaders;

public class Bunker {
	private int hitNum = 8, x, y;
	
	public Bunker(int xPos, int yPos)
	{
		this.x = xPos;
		this.y = yPos;
	}
	
	protected String getBunkerStatus() 
	{
		switch(hitNum)
		{
		case(8):
			return "src/bunker.png";
		case(7):
			return "src/bunkerhit1.png";
		case(6):
			return "src/bunkerhit2.png";
		case(5):
			return "src/bunkerhit3.png";
		case(4):
			return "src/bunkerhit4.png";
		case(3):
			return "src/bunkerhit5.png";
		case(2):
			return "src/bunkerhit6.png";
		case(1):
			return "src/bunkerhit7.png";
		}
		return "src/bunkerhit7.png";
	}
	
	protected void decrementhitNum()
	{
		this.hitNum--;
	}
	
	protected int gethitNum()
	{
		return this.hitNum;
	}
	
	protected void sethitNum(int x)
	{
		this.hitNum = x;
	}
	
	protected void setX(int xVal) 
	{
		x = xVal;
	}
	
	protected void setY(int yVal)
	{
		y = yVal;
	}
	
	protected int getX()
	{
		return this.x;
	}
	
	protected int getY()
	{
		return this.y;
	}

}
