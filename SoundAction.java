package spaceinvaders;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SoundAction implements ActionListener {

	private boolean sound = true;
	private Game_Board myGame;
	public SoundAction(Game_Board newGameBoard) {
		myGame = newGameBoard;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(sound)
		{
			myGame.setSoundOff();
			sound = false;
		}
		else
		{
			myGame.setSoundOn();
			sound = true;
		}

	}

}
