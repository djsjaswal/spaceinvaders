package spaceinvaders;

public class Defender extends Spacecraft {
	private int score = 0;
	private int lives = 4;

	protected void setScore(int x) {
		this.score = x;
	}

	protected String getScore() {
		return Integer.toString(this.score);
	}

	protected int getLives() {
		return this.lives;
	}

	public void setLives(int x) {
		this.lives = x;
	}
}
