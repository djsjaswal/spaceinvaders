package spaceinvaders;

/**
 * 
 * @author Archangel
 */

public abstract class Spacecraft {
	protected int x;
	protected boolean visible = true;
	protected int y;
	private int xSpeed = 10;
	private int ySpeed = 10;

	public boolean isVisible() {
		return visible;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}

	public void move(int x, int y) {
		this.x += xSpeed;
		this.y += ySpeed;
	}

}