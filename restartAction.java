package spaceinvaders;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class restartAction implements ActionListener {
	Game_Board runningGame;

	public restartAction(Game_Board newGameBoard) {
		runningGame = newGameBoard;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		runningGame.cleanUp();
		runningGame.restartGame();
	}

}
