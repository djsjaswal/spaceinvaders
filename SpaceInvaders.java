package spaceinvaders;

import java.awt.Dimension;
import java.io.IOException;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 * 
 * @author Archangel
 */
public class SpaceInvaders {
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {

			public void run() {
				JMenuItem quitGame;
				Game_Board newGameBoard = null;
				try {
					newGameBoard = new Game_Board();
				} catch (LineUnavailableException
						| UnsupportedAudioFileException | IOException
						| InvalidMidiDataException | MidiUnavailableException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				quitGame = new JMenuItem("Quit Game");
				JMenuItem restartGame = new JMenuItem("Restart Game");
				JFrame gameFrame = new JFrame("Space Invaders");
				JMenuBar menubar = new JMenuBar();
				JMenu gameControl = new JMenu("File");
				menubar.add(gameControl);
				gameControl.add(restartGame);
				gameControl.add(quitGame);
				JMenu sound = new JMenu("Sound");
				menubar.add(sound);
				JMenuItem soundStatus;
			    soundStatus = new JMenuItem("Sound On/Off");
			    sound.add(soundStatus);
				gameFrame.setJMenuBar(menubar);
				restartGame.addActionListener(new restartAction(newGameBoard));
				soundStatus.addActionListener(new SoundAction(newGameBoard));
				quitGame.addActionListener(new quitGameAction());
				gameFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				gameFrame.setPreferredSize(new Dimension(820, 650));
				gameFrame.getContentPane().add(newGameBoard);
				gameFrame.pack();
				gameFrame.setVisible(true);
			}
		});
	}
}
