package spaceinvaders;

import java.util.Random;

/**
*
* @author Archangel
*/

public class Alien extends Spacecraft {

	private boolean alive;
	@SuppressWarnings("unused")
	private int selectValue;
	Random generator = new Random();

	public Alien(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void act(int direction) {
		this.x += direction;
	}

	public boolean isAlive() {
		return this.alive;
	}

	public int getValue() {
		return this.selectValue = generator.nextInt(250);
	}
}