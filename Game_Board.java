package spaceinvaders;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Ellipse2D;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * 
 * @author Archangel
 */
public class Game_Board extends JPanel implements Runnable,
		MouseMotionListener, MouseListener {

	Random generator = new Random();
	private int selectValue;
	private Image player, alien, livesLabel, currentLives, bunker;
	private static final long serialVersionUID = 1L;
	private int direction = -10, x = 55, y = 50, gunX, bulletStop = 500,
			alienfireX, alienfireY, playerX, playerY = 500, scoreTracker = 0,
			moveValue = 1, incrementValue = 0, playerLivesX = 350,
			playerLivesY = 550, liveStatus, bunkerX = 100, level = 1, bg = 850,
			bgColor = 0, timerValue = 0, hsYVal = 335;
	private static int bunkerY = 380;
	private ArrayList<Alien> invaders;
	private ArrayList<Integer> highScores = new ArrayList<Integer>();
	private boolean fired = false, hit = false, alienfire = false,
			gameOn = false, gameOver = false, title = true, userHasHS = false,
			sound = true;
	private Defender newPlayer;
	private JLabel score = new JLabel();
	private JLabel playerScore = new JLabel();
	private JLabel highS = new JLabel();
	private JLabel titleWord = new JLabel();
	private JLabel startGame = new JLabel();
	private JLabel blank = new JLabel();
	private JLabel restartGame = new JLabel();
	private ArrayList<Bunker> bunkers = new ArrayList<Bunker>();
	private BufferedReader in;
	private BufferedWriter out;
	private ArrayList<Ellipse2D.Double> stars;
	private Timer blinkTimer;
	private Font startFont = new Font("Elephant", 1, 25);
	private Font TitleFont = new Font("Bodoni MT Black", 2, 70);
	private Clip clip, clip2;
	private AudioInputStream ais, ais2;
	private Sequencer musicPlayer;

	public Game_Board() throws LineUnavailableException,
			UnsupportedAudioFileException, IOException,
			InvalidMidiDataException, MidiUnavailableException {
		if (sound) {
			File url = new File("src/LaserBlaster.wav");
			File url2 = new File("src/explode.wav");
			clip = AudioSystem.getClip();
			ais = AudioSystem.getAudioInputStream(url);
			clip2 = AudioSystem.getClip();
			ais2 = AudioSystem.getAudioInputStream(url2);
			Sequence music = MidiSystem
					.getSequence(new File("src/StarWars.mid"));
			musicPlayer = MidiSystem.getSequencer();
			musicPlayer.setSequence(music);
			musicPlayer.open();
			musicPlayer.setLoopCount(Sequencer.LOOP_CONTINUOUSLY);
			musicPlayer.start();
			clip.open(ais);
			clip2.open(ais2);
		}
		try {
			in = new BufferedReader(new FileReader("src/file.txt"));
			// System.out.println(in.readLine());
			String line = in.readLine();
			while (line != null) {
				// //////////////////////////////////////////////////////////////////////
				highScores.add((Integer) Integer.parseInt(line));
				line = in.readLine();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		InitializeBackground(bg);
		super.setBackground(Color.BLACK);
		blank.setFont(TitleFont);
		blank.setForeground(Color.BLACK);
		blank.setText(" A ");
		titleWord.setFont(TitleFont);
		titleWord.setForeground(Color.RED);
		titleWord.setText("SPACE INVADERS");
		startGame.setFont(startFont);
		startGame.setForeground(Color.WHITE);
		startGame.setText("Start Game");
		startGame.addMouseListener(this);
		super.add(blank);
		super.add(titleWord);
		super.add(startGame);
	}

	public void destroyTitle() {
		restartGame.removeMouseListener(this);
		startGame.removeMouseListener(this);
		super.remove(restartGame);
		super.remove(titleWord);
		super.remove(startGame);
		super.remove(blank);
	}

	public void destroyBar() {
		super.remove(score);
		super.remove(playerScore);
		super.remove(highS);
	}

	private void startGame() {
		userHasHS = false;
		gameOver = false;
		title = false;
		gameOn = true;
		destroyTitle();
		newPlayer = new Defender();
		invaders = new ArrayList<Alien>();
		InitializeBunkers();
		InitializeBackground(bg);
		IntializeAliens(level);
		IntializeHUD();
		super.addMouseMotionListener(this);
		super.addMouseListener(this);
		new Thread(this).start();
	}

	private void IntializeHUD() {
		Font scoreFont = new Font("Arial", 1, 20);
		score.setFont(scoreFont);
		score.setForeground(Color.WHITE);
		score.setText("SCORE:");
		playerScore.setFont(scoreFont);
		playerScore.setForeground(Color.RED);
		playerScore.setText(newPlayer.getScore());
		highS.setFont(scoreFont);
		highS.setForeground(Color.WHITE);
		highS.setText("        HIGH SCORE: " + highScores.get(0).intValue()
				+ "           Level: " + level);
		super.add(score);
		super.add(playerScore);
		super.add(highS);
	}

	// Thanks warren!
	private void InitializeBackground(int i) {

		int amountStars = (820 * i) / 100;
		ArrayList<Ellipse2D.Double> tmp = new ArrayList<Ellipse2D.Double>();

		for (int count = 0; count < amountStars; count++)
			tmp.add(new Ellipse2D.Double((int) (Math.random() * 820) + 0,
					(int) (Math.random() * 850) + 0, 1, 1));

		stars = tmp;
	}

	private void InitializeBunkers() {
		int j;
		if(level < 3)
		{
			j = 1;
		}
		else if((level < 6) && (level > 2))
		{
			j = 2;
		}
		else
		{
			j = 3;
		}
		for (int i = 0; i < j; i++) {
			Bunker newBunker = new Bunker(bunkerX, bunkerY);
			bunkerX = bunkerX + 220;
			bunkers.add(newBunker);
		}
	}

	private void IntializeAliens(int lvl) {
		for (int k = 0; k < lvl; k++) {
			// 24 Invaders!
			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 4; j++) {
					Alien ainvader = new Alien(x, y);
					invaders.add(ainvader);
					x = x + 60;
				}
			}
			y = y + 30;
			x = 55;
		}
	}

	private void setDirection(int dirx) {
		this.direction = dirx;
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		if (gameOn) {
			if (e.getX() > 732) {
				playerX = 732;
			} else if (e.getX() < 8) {
				playerX = 8;
			} else {
				playerX = e.getX();
			}
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if ((title) || (gameOver)) {
			startGame();
		}

	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (gameOn) {
			if (fired == false) {
				if (sound) {
					clip.loop(0);
					clip.setMicrosecondPosition(0);
				}
				gunX = e.getX();
				fired = true;
				hit = false;
			}
		}

	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (timerValue >= 2) {
			blinkTimer.stop();
			timerValue = 0;
		}
		livesLabel = new ImageIcon("src/livesLabel.png").getImage();
		player = new ImageIcon("src/spacecraft.png").getImage();
		alien = new ImageIcon("src/alien.png").getImage();
		currentLives = new ImageIcon("src/playerLives.png").getImage();
		if (title) {
			drawBackground(g);
			g.setColor(Color.RED);
			g.setFont(startFont);
			g.drawString("Top Scores: ", 320, 300);
			g.setColor(Color.WHITE);
			for (int countx = 0; countx < highScores.size(); countx++) {
				g.drawString(" " + highScores.get(countx).intValue(), 320,
						hsYVal);
				hsYVal = hsYVal + 20;
			}
		}
		if (gameOn) {
			liveStatus = newPlayer.getLives();
			switch (liveStatus) {
			case (4):

				g.drawImage(currentLives, playerLivesX, playerLivesY, this);
				g.drawImage(currentLives, playerLivesX + 35, playerLivesY, this);
				g.drawImage(currentLives, playerLivesX + 70, playerLivesY, this);
				break;
			case (3):
				g.drawImage(currentLives, playerLivesX, playerLivesY, this);
				g.drawImage(currentLives, playerLivesX + 35, playerLivesY, this);
				break;
			case (2):
				g.drawImage(currentLives, playerLivesX, playerLivesY, this);
				break;
			}
			drawBackground(g);
			drawBunkers(g);
			drawPlayer(g);
			drawAlien(g);
			alienFire(g);
			g.setColor(Color.WHITE);
			g.fillRect(0, 540, 800, 5);
			g.drawImage(livesLabel, 550, 550, this);
			if (fired == true) {
				startGun(g);
			}
		}
		if (gameOver == true) {
			drawBackground(g);
			this.setBackground(Color.BLACK);
			g.setColor(Color.RED);
			g.setFont(startFont);
			g.drawString("GAME OVER", 255, 155);
			g.drawString("Please Click to Restart Game", 190, 255);
			if (userHasHS) {
				g.drawString("New Top Score! ", 155, 355);
				g.setColor(Color.WHITE);
				for (int countx = 0; countx < highScores.size(); countx++) {
					g.drawString(" " + highScores.get(countx).intValue(), 155,
							hsYVal);
					hsYVal = hsYVal + 20;
				}
			}
			for (int count = 0; count < highScores.size(); count++) {
				if (userHasHS == false) {
					if (Integer.parseInt(newPlayer.getScore()) > highScores
							.get(count).intValue()) {
						userHasHS = true;
						highScores.add(count,
								Integer.parseInt(newPlayer.getScore()));
						if (highScores.size() > 5) {
							highScores.remove(5);
						}
						// Write the top score to file!
						try {

							out = new BufferedWriter(new FileWriter(new File(
									"src/file.txt")));
							for (int countx = 0; countx < highScores.size(); countx++) {
								out.write(""
										+ highScores.get(countx).intValue());
								out.newLine();
								out.flush();
							}
						} catch (IOException e) {
							System.out.println("There was a problem:" + e);
						}
						break;
					}
				}

			}
		}
	}

	public void drawBackground(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		Iterator<Ellipse2D.Double> itr = stars.iterator();
		while (itr.hasNext()) {
			Ellipse2D.Double star = itr.next();
			g2.draw(star);
		}
		for(int count = 0; count < stars.size(); count++){
		       if(stars.get(count).y == 650)
		         stars.get(count).y = 0;
		       else
		         stars.get(count).y++;
		 }
	}

	public void drawBunkers(Graphics g) {
		Iterator<Bunker> itB = bunkers.iterator();
		while (itB.hasNext()) {
			Bunker newBunker = (Bunker) itB.next();
			if (newBunker.gethitNum() < 1) {
				itB.remove();
			}
			bunker = new ImageIcon(newBunker.getBunkerStatus()).getImage();
			g.drawImage(bunker, newBunker.getX(), 380, this);
		}
	}

	public void drawAlien(Graphics g) {
		Iterator<Alien> it = invaders.iterator();
		while (it.hasNext()) {
			Alien newinvader = (Alien) it.next();
			g.drawImage(alien, newinvader.getX(), newinvader.getY(), this);
		}
	}

	public void drawPlayer(Graphics g) {
		g.drawImage(player, playerX, 475, this);
	}

	public void startGun(Graphics g) {
		Iterator<Alien> it = invaders.iterator();
		Iterator<Bunker> itB = bunkers.iterator();
		while (it.hasNext() && fired == true && hit == false) {
			Alien newinvader = (Alien) it.next();
			// Checking if the bullet from player hits the alien
			if (((newinvader.getX() + 21) > gunX)
					&& ((newinvader.getX() - 21) < gunX)
					&& ((newinvader.getY() + 15) > bulletStop)
					&& ((newinvader.getY() - 15) < bulletStop)) {
				if (incrementValue > 10) {
					moveValue = moveValue + 1;
					incrementValue = 0;
				} else {
					incrementValue++;
				}
				it.remove();
				removeBullet(g);
				checkStatus();
				// 32 represents the number of aliens
				scoreTracker = scoreTracker + (level * 2) + invaders.size() + 1;
				scoreTracker++;
				newPlayer.setScore(scoreTracker);
			}
		}
		while (itB.hasNext() && fired == true && hit == false) {
			Bunker bunker1 = (Bunker) itB.next();
			if ((bulletStop < (bunker1.getY() + 16))
					&& (bulletStop > (bunker1.getY() - 16))
					&& (gunX < (bunker1.getX() + 75))
					&& (gunX > (bunker1.getX() - 23))) {
				bunker1.decrementhitNum();
				hit = true;
				fired = false;
			}
		}
		if ((fired == true) && (hit == false)) {
			g.drawRect(gunX + 23, bulletStop, 5, 5);
			g.setColor(Color.YELLOW);
			g.fillRect(gunX + 23, bulletStop, 5, 5);
			bulletStop = bulletStop - 5;
			if (bulletStop < 50) {
				removeBullet(g);
			}

		}
	}

	public void checkStatus() {
		Iterator<Alien> it = invaders.iterator();
		if (!it.hasNext()) {
			nextLevel(level);
		}
	}

	public void nextLevel(int i) {
		moveValue = 1;
		bunkerX = 100; // Reset bunker position to original
		cleanUp();
		incrementValue = 0;
		direction = -10;
		x = 55;
		y = 50;
		if (level > 10) {
			level = 10;
		} else {
			level = i + 1;
		}
		highS.setText("        HIGH SCORE: " + highScores.get(0).intValue()
				+ "           Level: " + level);
		InitializeBunkers();
		IntializeAliens(level);
		this.repaint();
	}

	public void cleanUp() {
		Iterator<Bunker> itB = bunkers.iterator();
		while (itB.hasNext()) {
			itB.next();
			itB.remove();
		}
	}

	public void removeBullet(Graphics g) {
		g.dispose();
		hit = true;
		fired = false;
		bulletStop = 500;
	}

	public int moveDownRight() {
		Iterator<Alien> firstIterator = invaders.iterator();
		while (firstIterator.hasNext()) {
			Alien invader1 = (Alien) firstIterator.next();
			if (invader1.getY() > 320) {
				Iterator<Bunker> itB = bunkers.iterator();
				while (itB.hasNext()) {
					itB.next();
					itB.remove();
				}
			}
			if (invader1.getY() > 425) {
				gameOver();
			}
			invader1.setY(invader1.getY() + 1);
		}
		return (moveValue * -1);
	}

	public int moveDownLeft() {
		Iterator<Alien> secondIterator = invaders.iterator();
		while (secondIterator.hasNext()) {
			Alien invader2 = (Alien) secondIterator.next();
			if (invader2.getY() > 320) {
				Iterator<Bunker> itB = bunkers.iterator();
				while (itB.hasNext()) {
					itB.next();
					itB.remove();
				}
			}
			if (invader2.getY() > 420) {
				gameOver();
			}
			invader2.setY(invader2.getY() + 1);
		}
		return (moveValue);
	}

	public void blink() {
		timerValue++;
		if (bgColor == 0) {
			super.setBackground(Color.RED);
			bgColor = 1;
		} else {
			super.setBackground(Color.BLACK);
			bgColor = 0;
		}

	}

	public void alienFire(Graphics g) {
		if (alienfire == true) {
			if (alienfireY < 510) {
				// Check if the alien bullet hits the player
				if ((alienfireY < (playerY + 15))
						&& (alienfireY > (playerY - 15))
						&& (alienfireX > (playerX - 10))
						&& (alienfireX < (playerX + 50))) {
					newPlayer.setLives(newPlayer.getLives() - 1);
					blinkTimer = new Timer(50, new ActionListener() {
						public void actionPerformed(ActionEvent arg0) {
							blink();
						}
					});
					if (sound) {
						clip2.loop(0);
						clip2.setMicrosecondPosition(0);
					}
					if (timerValue < 2) {
						blinkTimer.start();
					}
					if (newPlayer.getLives() <= 0) {
						gameOver();
					}
					alienfire = false;
				}
				// Bunker hit
				Iterator<Bunker> itb1 = bunkers.iterator();
				while (itb1.hasNext()) {
					Bunker bunker1 = (Bunker) itb1.next();
					if ((alienfireY < (bunker1.getY() + 39))
							&& (alienfireY > (bunker1.getY() - 39))
							&& (alienfireX < (bunker1.getX() + 98))
							&& (alienfireX > (bunker1.getX()))) {
						bunker1.decrementhitNum();
						alienfire = false;
					}
				}
				g.drawRect(alienfireX, alienfireY, 5, 5);
				g.setColor(Color.YELLOW);
				g.fillRect(alienfireX, alienfireY, 5, 5);
				alienfireY = alienfireY + 2;
			} else {
				alienfire = false;
			}
		} else {
			Iterator<Alien> it1 = invaders.iterator();
			while (it1.hasNext()) {
				Alien invader1 = (Alien) it1.next();
				if (selectValue == invader1.getValue()) {
					alienfire = true;
					alienfireX = invader1.getX();
					alienfireY = invader1.getY();
					alienFire(g);
				}
			}
		}
	}

	public void moveAliens() {
		Iterator<Alien> it1 = invaders.iterator();
		while (it1.hasNext()) {
			Alien invader1 = (Alien) it1.next();
			int x = invader1.getX();

			if (x >= 800 - 30 && direction != -10) {
				setDirection(moveDownRight());
			}

			if (x <= 30 && direction != 10) {
				setDirection(moveDownLeft());
			}
		}
		Iterator<Alien> it = invaders.iterator();
		while (it.hasNext()) {
			Alien invader3 = (Alien) it.next();
			if (invader3.isVisible()) {
				invader3.act(direction);
			}
		}
	}

	public void restartGame() {
		cleanUp();
		resetVars();
		userHasHS = false;
		gameOver = false;
		title = false;
		gameOn = true;
		destroyTitle();
		newPlayer = new Defender();
		invaders = new ArrayList<Alien>();
		InitializeBunkers();
		InitializeBackground(bg);
		IntializeAliens(level);
		IntializeHUD();
	}

	public void resetVars() {
		cleanUp();
		destroyBar();
		direction = -10;
		timerValue = 0;
		incrementValue = 0;
		scoreTracker = 0;
		moveValue = 1;
		level = 1;
		bunkerX = 100;
		x = 55;
		y = 50;
		hsYVal = 375;
	}

	public void gameOver() {
		resetVars();
		blinkTimer.stop();
		gameOn = false;
		title = false;
		gameOver = true;
		restartGame.setFont(startFont);
		restartGame.setForeground(Color.WHITE);
		restartGame.setText(" ");
		restartGame.setLocation(250, 250);
		restartGame.addMouseListener(this);
		super.add(restartGame);
		this.repaint();
	}

	@Override
	public void run() {

		while (gameOn) {
			selectValue = generator.nextInt(250);
			this.repaint();
			moveAliens();
			newPlayer.setScore(scoreTracker++);
			playerScore.setText(newPlayer.getScore());

			try {
				Thread.sleep(15);
			} catch (InterruptedException ex) {
				Logger.getLogger(Game_Board.class.getName()).log(Level.SEVERE,
						null, ex);
			}
		}
	}

	public void setSoundOn() {
		sound = true;
		musicPlayer.start();
	}

	public void setSoundOff() {
		sound = false;
		musicPlayer.stop();
	}
}
